module szl.proba02 {
    requires javafx.controls;
    requires javafx.fxml;

    opens szl.proba02 to javafx.fxml;
    exports szl.proba02;
}