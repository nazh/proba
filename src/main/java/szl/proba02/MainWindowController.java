package szl.proba02;

import java.io.IOException;
import javafx.fxml.FXML;

public class MainWindowController {

    @FXML
    private void switchToSecondary() throws IOException {
        App.setRoot("secondary");
    }
}
